import sqlite3
import json
import smtplib
from email.mime.text import MIMEText

contactdb = sqlite3.connect("data/contacts.db")
contactdbc = contactdb.cursor()

newmessages = contactdbc.execute("select * from Contacts where status='new'")
msg = ""

for message in newmessages:
    msg += "Message From: " + message[1] + "\n"
    msg += "Contact: " + message[2] + (" / " if len(message[2]) != 0 and len(message[3]) != 0 else " ") + message[3] + "\n"
    msg += message[4] + "\n"
    msg += "- - -" + "\n"

mailmsg = MIMEText(msg)

# if(len(msg) == 0):
    # sys.exit(-1)

configf = open("config.json", 'r')
configs = configf.readlines()

config = json.loads("".join(configs))
configf.close()

mailmsg['Subject'] = config["mailinfo"]["subject"]
mailmsg['From'] = config["mailinfo"]["from"]
mailmsg['to'] = config["mailinfo"]["to"]

conn = smtplib.SMTP_SSL(config["mailinfo"]["server"])
conn.login(config["mailinfo"]["username"], config["mailinfo"]["password"])

try:
    conn.sendmail(config["mailinfo"]["from"], config["mailinfo"]["to"], mailmsg.as_string())
finally:
    conn.quit()
    newmessages = contactdbc.execute("UPDATE Contacts SET status='sent' WHERE status='new' ")
    contactdb.commit()
    contactdbc.close()
