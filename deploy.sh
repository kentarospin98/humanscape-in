source ./setup.sh

export WORKERS=$(cat config.json | jq ".serverinfo .workers" -r)
export PORT=$(cat config.json | jq ".serverinfo .port" -r)
export HOST=$(cat config.json | jq ".serverinfo .host" -r)
export LOGFILE=$(cat config.json | jq ".serverinfo .logfile" -r)
export KEYFILE=$(cat config.json | jq ".serverinfo .keyfile" -r)
export CERTFILE=$(cat config.json | jq ".serverinfo .certfile" -r)

export KEYFILEARG=""
export CERTFILEARG=""

if [ -z "$KEYFILE" ]; then
  echo "No KEYFILE used"
else
  if [ -s $KEYFILE ] && [ -r $KEYFILE ]; then
    echo "Keyfile : $KEYFILE"
    export KEYFILEARG="--keyfile $KEYFILE"
  else
    echo "Keyfile unreadable"
  fi;
fi;

if [ -z "$CERTFILE" ]; then
  echo "No CERTFILE used";
else
  if [ -s $CERTFILE ] && [ -r $CERTFILE ]; then
    echo "Certfile : $CERTFILE";
    export CERTFILEARG="--certfile $CERTFILE";
  else
    echo "Certfile unreadable"
  fi;
fi;

if [ -z "$CERTFILEARG" ] || [ -z "$CERTFILEARG" ]; then
  echo "SSL Disabled"
else
  echo "SSL enabled"
  export SSLARG="$KEYFILEARG $CERTFILEARG"
fi;

echo $SSLARG

if [ -z "$PORT" ]; then
  export PORT=$(if [ -z $SSLARG ]; then echo 80; else echo 443; fi;)
  echo "No Port specified. Defaulting to $PORT"
fi;

if [ -z "$HOST" ]; then
  export HOST=0.0.0.0
  echo "No Host specified. Defaulting to $PORT"
fi;

if [ -z "$WORKERS" ]; then
  export WORKERS=1
  echo "No worker count specified. Defaulting to $WORKERS Workers"
fi;

if [ -z "$LOGFILE" ]; then
  export LOGFILE=server.log
  echo "No Logfile specified. Defaulting to $LOGFILE"
fi;

echo "Running Server on $HOST:$PORT with $WORKERS Workers"

gunicorn application:app --workers=$WORKERS --bind=$HOST:$PORT --access-logfile $LOGFILE $SSLARG
