if [ -x /usr/bin/python3 ]; then
  export PYTHON=/usr/bin/python3;
else
  export PYTHON=python3;
fi;

virtualenv -p $PYTHON .

if [ $? -ne 0 ]; then
  exit -2
fi

mkdir data

source ./bin/activate
pip3 install flask gunicorn pysqlite3 jq
