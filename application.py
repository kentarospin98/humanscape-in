from flask import Flask, redirect, render_template, request, url_for, send_from_directory
import sqlite3
import re
import json

configf = open("config.json", 'r')
configs = configf.readlines()

config = json.loads("".join(configs))
configf.close()
# config = json.load("config.json")

app = Flask(__name__)

contactdb = sqlite3.connect("data/contacts.db")
contactdb.row_factory = sqlite3.Row

def createcontacttable():
    contactdbc = contactdb.cursor()
    contactdbc.execute("""CREATE TABLE `Contacts` (
    `ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `name`	TEXT NOT NULL,
    `email`	TEXT,
    `phone`	TEXT,
    `message`	TEXT,
    `status`	TEXT
    );""")
    contactdb.commit()
    contactdbc.close()

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/contact", methods=["GET", "POST"])
def contact():
    if request.method == "GET":
        return render_template("contact.html", gapikey=config["googlemaps"]["apikey"])
    elif request.method == "POST":
        name = request.form.get("name") if request.form.get("name") else False
        email = request.form.get("email") if request.form.get("email") else False
        phone = request.form.get("phone") if request.form.get("phone") else False
        message = request.form.get("message") if request.form.get("message") else False

        if not name:
            return "Please Provide a name"

        if phone:
            phone = re.sub("[^0-9]", "", phone)
            if len(phone) > 20:
                return "Phone number too long"

        if not email and not phone:
            return "Please provide either an email or a phone number"

        emailpat = re.compile("^[\w\.]+@([\w]+\.)+[\w]{2,4}$")

        if email and not emailpat.match(email):
            return "Please input a valid email"

        contactdbc = contactdb.cursor()
        contactdbc.execute("INSERT INTO 'Contacts' (name, email, phone, message, status) VALUES (?,?,?,?,?)", [name, email if email else "", phone if phone else "", message if message else "", "new"])
        contactdb.commit()
        contactdbc.close()

        return render_template("confirmation.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/favicon.ico")
def favicon():
    return send_from_directory("static", "images/favicon.ico")

if __name__ == '__main__':
    app.run(host=config["serverinfo"]["host"], port=config["serverinfo"]["port"])

contactdbc = contactdb.cursor()
if len([x for x in contactdbc.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='Contacts'")]) < 1:
    createcontacttable()
else:
    f = contactdbc.execute("SELECT * FROM Contacts")
    fl = [description[0] for description in f.description]
    if not ("ID" in fl and "name" in fl and "email" in fl and "phone" in fl and "message" in fl and "status" in fl):
        createcontacttable()
        contactdbc.close()
